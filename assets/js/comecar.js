var comecar = (function() {
    function init() {
        base_url = location.origin + '/vibbrado/';
        
        $counterTotal = $('#count-total');
        $counterDos = $('#count-dos');
        $counterDonts = $('#count-donts');

        user_id = $('#user_id').val();

        $todosId = $('#todos_id');
        $todosUrl = $('#todos_url');
        $todoList = $('#todoList');
        $todosBtnGravar = $('#todosSave');

        $removeTodo = $('#removeTodo');
        $removeTodos = $('#removeTodos');

        $('#todoText').focus();

        $('body').delegate('#removeTodos', 'click', function(event) {
            todos.methods.removeTodos(todos.data, function(response) {
                if (response.status == 'success') {
                    alertify.message('Todo removido, comece novamente com outra url');
                    window.location.href = base_url;
                }
            });
        });

        $('body').delegate('#addSubTodo', 'click', function(event) {
            let $this = $(event.target);
            let item = {
                id: '',
                text: 'Meu primeiro subtodo',
                done: 0,
                parent: $this.parent().parent().children('input').attr('id')
            };
            todos.methods.saveItem(item, function(response) {
                if (response.status === 'success') {
                    let $el = $this.parent().parent().children('.list-group').removeClass('hidden');
                    $el.append(todos.methods.template(response.data));
                    todos.methods.addItem(response.data);
                } else {
                    alertify.message('Deu bug ao criar todos');
                }
            });
        });

        $('body').delegate('#addTodo', 'click', function(event) {
            let item = {
                id: '',
                text: $('#todoText').val(),
                done: 0,
                parent: 0
            };
            let $this = $(event.target);
            $this.children('i').addClass('fa-spin');
            todos.methods.saveItem(item, function(response) {
                if (response.status === 'success') {
                    $this.children('i').removeClass('fa-spin');
                    $('#todoText').val('');
                    EVT.emit('todoRender', todos.methods.addItem(response.data));
                } else {
                    alertify.warning('Deu bug ao criar todos');
                }
            });
            
        });

        $('body').delegate('.list-group-box-done', 'click', function(event) {
            let item = {
                id: event.target.id,
                text: $(event.target).parent().children('div').text().trim(),
                done: (event.target.checked) ? 1 : 0,
                parent: event.target.parent
            };
            todos.methods.toggleDone(item, function(response) {
                if (response.status ==='success') {
                    EVT.emit('updateCounter');
                } else {
                    alertify.warning('Erro ao atualizar todo');
                }
            });
        });

        $('body').delegate('#todos_url', 'input', function(event) {
            let preview = event.target.value;
            $('#todos_url_preview').text(preview);
        });

        $('body').delegate('#todo_text', 'focusout', function(event) {
            let item_temp = {
                id: $(event.target).next('input').attr('id'),
                text: event.target.innerText,
                done: ($(event.target).next('input').is(':checked')) ? 1 : 0,
                parent: $(event.target).next('input').attr('data-parent')
            };
            todos.methods.saveItem(item_temp, function(response) {
                todo = todos.data.itens.filter(function(todo_item) {
                    return todo_item.id == response.data.id;
                })[0];
                todo.text = item_temp.text;
            });
        });

        $('body').delegate('#todosSave', 'click', function(event) {
            todos.methods.save(todos.data);
        });

        $('body').delegate('#removeTodo', 'click', function(event) {
            let $this = $(event.target);
            let todos_id = todos.data.id;
            let todo_id = $this.parent().parent().children('input').attr('id');
            todos.methods.removeTodo({todos_id: todos_id, todo_id: todo_id}, function(response) {
                if (response.status == 'success') {
                    todos.data.itens = todos.data.itens.filter(function(item) {
                        return item.id != response.data.todo_id;
                    });
                    let $el = $this.parent().parent();
                    EVT.emit('animateCSS', $el, 'lightSpeedOut', function(){
                        $el.remove();
                    });
                } else {
                    alertity.message('Deu bug ao remover todo');
                }
            });
        });

        EVT.emit('todosRender');
        EVT.emit('todosAdjust');
        console.log('Iniciando: Começar... ok!');
    }

    function todosAdjust() {
        let $li = $todoList.children('li');
        if ($li.length) {
            let $tempItem = '';
            let tempItens = [];
            $li.each(function(index, item) {
                let $item = $(item);
                if ($item.children('input').attr('data-parent') > 0) {
                    $tempItem = $item;
                    tempItens.push($tempItem);
                    $item.remove();
                }
            });
            $li.each(function(index, item) {
                tempItens.map(function($item_sub) {
                    if ($item_sub.children('input').attr('data-parent') == $(item).children('input').attr('id')) {
                        $(item).children('ul').removeClass('hidden').append($item_sub);
                    }                    
                });
            });
        }
    }

    function todosRender() {
        todos.data.id = $todosId.val();
        todos.data.url = $todosUrl.val();
        // se lista ja tem todo, popular todos.data.itens
        // se nao tem todo, carregar default
        if ($todoList.children('li').length) {
            todos.data.itens.pop(0);
            $todoList.children('li').each(function(i,item) {
                let tempItem = {
                    id: $(item).children('div').next('input').attr('id'),
                    text: $(item).children('div').text(),
                    done: ($(item).children('div').next('input').is(':checked') ? 1 : 0),
                    parent: $(item).children('div').next('input').attr('data-parent')
                };
                todos.data.itens.push(tempItem);
            });
        }
        EVT.emit('updateCounter');
    }

    function todoRender(item) {
        $todoList.append(todos.methods.template(item));
        EVT.emit('updateCounter');
    }

    function updateCounter() {
        let scoreAll = todos.methods.countAll;
        let scoreDos = todos.methods.countDos;
        let scoreDonts = todos.methods.countDonts;
        
        $counterTotal.text(scoreAll);
        $counterDos.text(scoreDos);
        $counterDonts.text(scoreDonts);
    }

    function animateCSS($el, animationName, callback) {
        $el.addClass('animated ' + animationName);
        function handleAnimationEnd() {
            $el.removeClass('animated ' + animationName);
            $el.unbind('animationend', handleAnimationEnd)
            if (typeof callback === 'function') callback()
        }
        $el.bind('animationend', handleAnimationEnd)
    }

    var todos = {
        data: {
            url: '',
            id: '',
            itens : []
        },
        methods: {
            addItem: function(item) {
                if (todos.data.itens.push(item)) {
                    return item;
                }
            },
            removeItem: function(item_id) {

            },
            removeTodo: function(data, callback) {
                $.post(base_url + '/api/todo_delete/', data, function(response) {
                    callback(response);
                });
            },
            removeTodos: function(data, callback) {
                $.post(base_url + '/api/todos_delete/', data, function(response) {
                    callback(response);
                });
            },
            saveItem: function(item, callback) {
                $.post(base_url + '/api/todo_update/',{data: {item:item, todos_id: $todosId.val()}}, function(response) {
                    callback(response);
                });   
            },
            toggleDone: function(item, callback) {
                $.post(base_url + '/api/todo_update/',{data: {item:item}}, function(response) {
                    if (response.status === 'success') {
                        todo = todos.data.itens.filter(function(todo_item) {
                            return todo_item.id == response.data.id;
                        })[0];
                        todo.done = item.done;
                    }
                    callback(response);
                });
            },
            countAll: function() {
                return todos.data.itens.length;
            },
            countDos: function() {
                return todos.data.itens.filter(function(item) {
                    return item.done == 1;
                }).length;
            },
            countDonts: function() {
                return todos.data.itens.filter(function(item) {
                    return item.done == 0;
                }).length;
            },
            template: function(item) {
                return '<li class="list-group-item animated lightSpeedIn">'
                    + '<div id="todo_text" contenteditable="true">' + item.text + '</div>'
                    + '<input class="list-group-box-done" ' + (item.done == false ? '' : 'checked') + ' id="' + item.id + '" type="checkbox" value="' + (item.done == false ? 'on' : 'off') + '">'
                    + '<a href="#" id="removeTodo" class="pull-right">'
                    + '<i class="fa fa-trash"></i>'
                    + '</a>'
                    + '<a href="#" id="addSubTodo" class="pull-right">'
                    + '<i class="fa fa-plus-circle"></i>'
                    + '</a>'
                    + '<ul class="hidden list-group"></ul>'
                    + '</li>';
            }
        }
    };

    var base_url;
    var $counterTotal, $counterDos, $counterDonts;
    var user_id;
    var $todosId, $todosUrl, $todoList;
    var $removeTodo, $removeTodos;
    
    EVT.on("init",init);
    EVT.on("animateCSS",animateCSS);
    EVT.on("updateCounter",updateCounter);
    EVT.on('todosRender', todosRender);
    EVT.on('todosRender', todosAdjust);
    EVT.on('todoRender', todoRender);
	return {};
})();