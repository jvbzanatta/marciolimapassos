var home = (function() {
    function init() {
        base_url = location.origin + '/vibbrado/';
        $todosUrlPreview = $('#todos_url_preview');
        $todosUrlGo = $('#todos_url_go');
        // as the user types, update the input text display
        $('body').delegate('#todos_url', 'input', function(event) {
            let preview = event.target.value;
            $todosUrlPreview.text(preview);
        });
        // whenever the user focus the url input, make sure the continue/create button is disabled
        // so we can check for a valid url later when he leaves focus or verify 
        $('body').delegate('#todos_url', 'focus', function(event) {
            $todosUrlGo.prop('disabled', true).addClass('disabled');
        });
        // as the user focus out from the input check if the url has disallowed chars
        $('body').delegate('#todos_url', 'focusout', function(event) {
            let $this = $(event.target);
            var expression = "^[^>#*@:/!)()]+$";
            var regex = new RegExp(expression);
            var t = $todosUrlPreview.text().trim();
            // if the url has disallowed chars
            if (!t.match(regex)) {
                // be sure the continue button is disabled
                $todosUrlGo.prop('disabled', true).addClass('disabled');
                $this.focus();
                alertify.warning('Essa url tem caracteres não permitidos, escolha outra', 1.5);
            // url is free from disallowed chars
            } else {
                // so, check if it is already assigned to a todos, 
                $.post(base_url + '/api/todos_get/', {
                    url: $todosUrlPreview.text().trim()
                }, function(response){
                    console.log(response);
                    if (response.status == 'success') {
                        // there is no todos list assigned to that url
                        alertify.message('Esta url está disponível e você pode começar...', 1.5);
                        $todosUrlGo.removeAttr('disabled').removeClass('disabled');
                    } else {
                        // there is already a todos list assigned to that url
                        $this.focus();
                        alertify.warning('Já existe uma lista com essa url, pense em outra ou edite a atual ;)',1.5);
                    }
                });
            }
        });
        // when the user clicks the continue/create todos list button... redirect him to his newest todos list
        $('body').delegate('#todos_url_go', 'click', function(event) {
            event.preventDefault();
            window.location.href=$('#todos_url_prefix').text().trim();
        });
        console.log('Loading: Home... ok!');
    }
   
    var base_url;
    var $todosUrlPreview, $todosUrlGo;
    
    EVT.on("init",init);
	return {};
})();