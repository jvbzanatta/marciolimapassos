# Vibbra.do

O Vibbra.do é um pequeno aplicativo web que permite a criação e edição de listas por fazer, as famosas "todo-lists". Além disso, é possível compartilhá-las 
de maneira divertida, pois elas ficam atreladas a uma url. 

### Tecnologias

A ferramenta foi desenvolvida em PHP, com o framework Code Igniter no back-end, e em Javascript/jQuery no front-end, com a ajuda do framework de animações
animate.css (Daniel Eden) e também da biblioteca Javascript EventEmitter (Kyle Simpson).

```
O aplicativo faz uso de cookies para gerenciar usuários e listas de forma rápida
```

### Instalação

* Importar o banco de dados 'vibbrado-db-final.sql'
* Copiar arquivos para a pasta 'vibbrado' no servidor web
* Configurar 'application/database.php' com informações do servidor de banco de dados
* Acessar http://seu-servidor/vibbrado e pode começar!

#### Demo

* [raiz] http://www.instadev.com.br/vibbrado/
* [exemplo todos-list] http://www.instadev.com.br/vibbrado/todos/meu-primeiro-todo

## Avaliação do Escopo

Está tudo certo com o escopo a primeira vista, portanto vou continuar. Durante o desenvolvimento tive algumas dúvidas que foram esclarecidas junto ao cliente por e-mail. 
De início, achei que fosse real-time o sistema, o que não foi confirmado pelo cliente, então segui com uma aplicação normal, porém no meio do trabalho fui deixando tudo preparado para implementar o real-time usando um servidor express com socket.io e nodejs para gerenciar as mensagens e canais, entretanto, não deu tempo e fica pra uma próxima atualização. 
Não foi possível também, deixar os subitens arrastáveis entre os itens da lista de todos, não deu tempo. Acho que com mais algumas horas conseguiria entregar 100%, mas não deu. 
O sistema foi testado ao ser desenvolvido e todas as telas entregues estão funcionais.

Foi incluído no escopo o item INÍCIO, referente a definição, criação e configuração do ambiente e das tecnologias de desenvolvimento do projeto.

## Estimativa em horas

### Início

Definir, criar e configurar esqueleto do projeto no ambiente de desenvolvimento.

```
Tempo estimado: 1h
```

### Tela Inicial
 
* Deve ser acessada na URL base do site (raiz) e conter explicações de como criar um nova lista, editar uma já existente e excluir
* O menu deve conter os seguintes itens:
  * Sobre: explicar o que faz a ferramenta 

```
Tempo estimado: 1h
```

### Criar Todo

* Ao entrar com uma URL qualquer, a mesma deve ser usada para se referenciar a um novo ToDo List
* Após a criação, o usuário deve ser enviado a tela de edição da ToDo 	

```
Tempo estimado: 4h
```

### Editar Todo

* Utilizando uma URL já associada a um ToDo, o usuário poderá realizar as seguintes operações em uma lista de itens de texto:
* Criar um novo item
* Editar um item 
* Apagar um item existente
* Organizar o item como sub-item de um item existente
* Mover um sub-item para fora do item pai, transformando-o em um outro item pai ou um sub-item de outro item; 

```
Tempo estimado: 8h
```

### Excluir Todo

* Como usuário posso excluir um ToDo que seja meu 

```
Tempo estimado: 1h
``` 

### Compartilhar Todo

* Como usuário gostaria de compartilhar a URL do ToDo que estou editando por email para uma ou mais pessoas

```
Tempo estimado: 1h
```

## Estimativa em DIAS do prazo de entrega

### Início, Tela Inicial e Criar Todo

```
Tempo estimado: 1 a 2 dias
```

### Editar Todo, Excluir Todo e Compartilhar Todo

```
Tempo estimado: 3 a 4 dias
```

## Aponte as horas e atividade que utilizou para fazer o teste

A planilha de controle está disponível neste repositório: 
```
Controle de horas - Marcio de Lima Passos
```
