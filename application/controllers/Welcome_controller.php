<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome_controller extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public $user_id ;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('User_model');
		$this->load->helper('cookie');
		// check user
		if ($this->input->cookie('vibbrado', true)) {
			$this->user_id = $this->input->cookie('vibbrado');
		} else {
			$user = [];
			$this->user_id = $this->User_model->save($user);
			if ($this->user_id) {
				$cookie = array(
					'name'   => 'vibbrado',
					'value'  => $this->user_id,
					 'expire' => '214748364'
				);
				$this->input->set_cookie($cookie);
			} else {
				echo 'erro ao registrar usuário';
			}
		}
	}
	
	public function index()
	{
		$this->page_title .= 'Bem-vindo';
		$this->data['meta']['content'] = $this->page_title;
		$data_header['meta'] = $this->data['meta'];
		// MENU
		$data_header['menu'] = $this->data['menu'];
		// CSS
		$data_header['css']=array(
			array('file' => base_url() . 'assets/estilos/estilos-admin.css'),
			array('file' => base_url() . 'assets/estilos/alertify.core.css'),
			array('file' => base_url() . 'assets/estilos/alertify.theme.bootstrap.css')
			); 
		// JS
		$data_footer['js']=array(
			array('file' =>  base_url() . 'assets/js/alertify.min.js'),
			array('file' =>  base_url() . 'assets/js/eventemitter2.js'),
			array('file' =>  base_url() . 'assets/js/app.js'),
			array('file' =>  base_url() . 'assets/js/home.js')
		);
	
		$this->load->view('header_view',$data_header);
		$this->load->view('home/content_view');
		$this->load->view('footer_view',$data_footer);	
	}
	public function sobre()
	{
		$this->page_title .= 'Sobre';
		$this->data['meta']['content'] = $this->page_title;
		$data_header['meta'] = $this->data['meta'];
		// MENU
		$data_header['menu'] = $this->data['menu'];
		// CSS
		$data_header['css']=array(
			array('file' => base_url() . 'assets/estilos/estilos-admin.css')
			); 
		// JS
		$data_footer['js']=array();
	
		$this->load->view('header_view',$data_header);
		$this->load->view('sobre/content_view');
		$this->load->view('footer_view',$data_footer);	
	}
	public function todos()
	{
		$this->page_title .= 'Sobre';
		$this->data['meta']['content'] = $this->page_title;
		$data_header['meta'] = $this->data['meta'];
		// MENU
		$data_header['menu'] = $this->data['menu'];
		// CSS
		$data_header['css']=array(
			array('file' => base_url() . 'assets/estilos/estilos-admin.css'),
			array('file' => base_url() . 'assets/estilos/animate.css'),
			array('file' => base_url() . 'assets/estilos/alertify.core.css'),
			array('file' => base_url() . 'assets/estilos/alertify.theme.bootstrap.css')
		); 
		// JS
		$data_footer['js']=array(
			array('file' =>  base_url() . 'assets/js/alertify.min.js'),
			array('file' =>  base_url() . 'assets/js/eventemitter2.js'),
			array('file' =>  base_url() . 'assets/js/app.js'),
			array('file' =>  base_url() . 'assets/js/comecar.js')
		);

		$data_content['todosExiste'] = false;
		$data_content['todosIsOwnedByUser'] = false;
		$data_content['todosUrl'] = $this->uri->segment(2);
		$data_content['user_id'] = $this->user_id;

		$this->load->helper('string');

		$this->load->model('User_model');
		$this->load->model('User_todos_model');
		$this->load->model('Todos_model');
		$this->load->model('Todos_todo_model');
		$this->load->model('Todo_model');
		
		// verificar se url existe
		$todos_data = array(
			'url' => $data_content['todosUrl']
		);
		$todos = $this->Todos_model->get_by($todos_data);
		// caso existe a url, buscar usuario associado
		if ($todos) {
			$data_content['todosExiste'] = TRUE;
			$user_todos = $this->User_todos_model->get_by(
				array(
					'todos_id' => $todos[0]->id
				)
			);
			if ($user_todos[0]->user_id == $this->user_id) {
				// é dono do todos
				$data_content['todosIsOwnedByUser'] = TRUE;
			}
			// carrega itens do todos
			$todos_todo_data = array(
				'todos_id' => $todos[0]->id
			);
			$todos_todo = $this->Todos_todo_model->get_by($todos_todo_data);
			if ($todos_todo) {
				$data_content['todo'] = array();
				foreach ($todos_todo as $key => $todo) {
					array_push($data_content['todo'], $this->Todo_model->get_by(array(
						'id' => $todo->todo_id
					))[0]);
				}
				// carrega lista de todo
			} 
			$data_content['todos'] = $todos;
			$data_content['todos_todo'] = $todos_todo;
		} else {
			// caso nao exista a url / todos
			// criar todos
			$todos_id = $this->Todos_model->save($todos_data);
			if ($todos_id) {
				// associar o novo todos ao usuario
				$user_todos = array(
					'user_id' => $this->user_id,
					'todos_id' => $todos_id
				);
				$user_todos_id = $this->User_todos_model->save($user_todos);
				if (!$user_todos_id) {
					echo '<br>erro ao associar todos ao usuário';
				}
				$data_content['todos'][] = (object) array(
					'id' => $todos_id
				);
				$data_content['todosIsOwnedByUser'] = TRUE;
			} else {
				echo 'erro ao criar todos';
			}
			
		}

		$this->load->view('header_view',$data_header);
		$this->load->view('todos/edit_view', $data_content);
		$this->load->view('footer_view',$data_footer);	
	}
}
