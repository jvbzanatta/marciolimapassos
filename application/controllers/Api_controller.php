<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_controller extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct()
	{
		parent::__construct();
    }   
    
    public function todos_get($data=null) {
        $data = $this->input->post();
        $this->load->model('Todos_model');
        $todos = $this->Todos_model->get_by($data);
        if ($todos) {
            $this->jsonOutput('fail', (object) $todos);
        } else {
            $this->jsonOutput('success', (object) $todos);
        }
    }

    public function todo_update($todo=null) {
        $todo = $this->input->post();
        $this->load->model('Todo_model');
        $this->load->model('Todos_todo_model');
        if ($todo['data']['item']['id']) {
            if (!$todo_atualizado_id = $this->Todo_model->save($todo['data']['item'], $todo['data']['item']['id'])) {
                $this->jsonOutput('fail', $todo['data']['item']);
            }
        } else {
            $todo_novo_id = $this->Todo_model->save($todo['data']['item']);
            $todo['data']['item']['id'] = $todo_novo_id;
            $todos_todo_novo = $this->Todos_todo_model->save(
                array(
                    'todos_id' => $todo['data']['todos_id'],
                    'todo_id' => $todo_novo_id
                )
            );
        }
        $this->jsonOutput('success', $todo['data']['item']);
    }
    
    public function todo_delete($id=null) {
        $data = $this->input->post();
        $this->load->model('Todo_model');
        $this->load->model('Todos_todo_model');
        // remove todo
        $this->Todo_model->delete($data['todo_id']);
        // remove associação de todos
        $todos_todo_remove_id = $this->Todos_todo_model->get_by($data);
        $this->Todos_todo_model->delete($todos_todo_remove_id[0]->id);
        $this->jsonOutput('success', (object) $data);
    }

    public function todos_delete($data=null) {
        $data = $this->input->post();

        $this->load->model('User_todos_model');
        $this->load->model('Todos_model');
        $this->load->model('Todo_model');
        $this->load->model('Todos_todo_model');
        // remove todos
        $this->Todos_model->delete($data['id']);
        // se tiver todo na lista todos, remover
        if (isset($data['itens'])) {
            foreach($data['itens'] as $key => $todo) {
                $this->Todo_model->delete($todo['id']);
                // remove associação de todos com todo
                $todos_todo_remove_id = $this->Todos_todo_model->get_by(array(
                    'todos_id' => $data['id'],
                    'todo_id' => $todo['id']
                ));
                $this->Todos_todo_model->delete($todos_todo_remove_id[0]->id);
            }
        }
        // remove associacao do todos com usuário
        $user_todos_remove_id = $this->User_todos_model->get_by(array(
            'todos_id' => $data['id']
        ));
        $this->User_todos_model->delete($user_todos_remove_id[0]->id);
        $this->jsonOutput('success', (object) $data);
    }

    public function todos_update($todos=null) {
        $todos = $this->input->post();
        // se ja tiver itens, atualizar
        // se nao tiver itens, criar os itens e associar ao todos
        $this->load->model('Todos_todo_model');
        $this->load->model('Todo_model');
        $todos_todo = $this->Todos_todo_model->get_by(
            array(
                'todos_id' => $todos['id']
            )
        );
        // atualiza os atuais
        // insere os novos
        foreach ($todos['itens'] as $key => $item) {
            if ($item['id']) {
                $todo_id = $this->Todo_model->save($item, $item['id']);
                if (!$todo_id) {
                    $this->jsonOutput('fail', $todos);
                }
            } else {
                // cria todo e asssocia
                unset($item['id']);
                $todo_id = $this->Todo_model->save($item);
                $todos['itens'][$key]['id'] = $todo_id;
                $todos_todo_novo = $this->Todos_todo_model->save(
                    array(
                        'todos_id' => $todos['id'],
                        'todo_id' => $todo_id
                    )
                );
                if (!$todos_todo_novo) {
                    $this->jsonOutput('fail', $todos);
                }
            } 
        }
		$this->jsonOutput('success', $todos);
	}
}