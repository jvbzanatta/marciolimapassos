<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Todos_model extends MY_Model {
    protected $_table_name = 'todos';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = 'id ASC';
    protected $_rules = array();
    protected $_timestamps = TRUE;
}