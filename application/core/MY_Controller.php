<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public $data = array();
	public $site_name;
	public $page_title;
	// public $isGLOBAL;
	
	public function __construct() {
		parent::__construct();

		$this->site_name = 'Vibbrado 1.0';
		$this->page_title = $this->site_name . ' | ';

		$this->data['meta'] = array(
			"name" => "title",
			"content" => $this->page_title
		);

        $this->data['menu'] = array(
            // array(
            //     "name" => "Amostras",
            //     "link" => '#',
            //     "class" => "dropdown",
            //     "elements" => array(
            //         array(
            //             "name" => "Amostras de Campo",
            //             "link" => base_url() . 'amostras/campo',
            //             "class" => ""
            //         ),
            //         array(
            //             "name" => "Amostras de Laboratório",
            //             "link" => base_url() . 'amostras/laboratorio',
            //             "class" => ""
            //         ),
            //         array(
            //             "name" => "Amostras de Testemunho",
            //             "link" => base_url() . 'amostras/testemunho',
            //             "class" => ""
            //         )
            //     )
			// ),
			array(
                "name" => "Início",
                "link" => base_url() . '',
                "class" => ""
            ),
            array(
                "name" => "Sobre",
                "link" => base_url() . 'sobre',
                "class" => ""
            )
        );
	}
	
	public function index()	{}

	public function show_dump($value) {
        echo "<pre>";
            var_dump($value);
        echo "</pre>";
	}
	
	public function notificar($usuario, $titulo, $mensagem) {
		$this->load->library('email');
		
		$this->email->set_newline("\r\n");
		$this->email->from('marciopassosbel@gmail.com', 'Márcio Passos'); // change it to yours
	    $this->email->to($usuario->email);// change it to yours
	    $this->email->subject($titulo);
	    $this->email->message($mensagem);

		if (!$this->email->send()) {
	    	return false;
	    } else {
			return true;
		}
	}

	public function checkJSON() {
		if (!$this->input->post()) {
			return false;
		} else {
			return true;
		}
	}

	public function jsonOutput($status, $obj) {
		$this->output
		->set_content_type('application/json')
		->set_output(json_encode(array(
			"status" => $status,
			"data" => $obj,
		)));
	}
}
