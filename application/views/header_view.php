<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <?php echo  "<meta name='" . $meta['name'] . "' content='" . $meta['content'] . "'>\n"; ?>

    <meta name="author" content="Vibbrado 1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="http://vibbra.com.br/src/images/fav/favicon.ico?v=4">
    <link rel="icon" sizes="16x16 32x32 64x64" href="http://vibbra.com.br/src/images/fav/favicon.ico?v=4">
    <link rel="icon" type="image/png" sizes="196x196" href="http://vibbra.com.br/src/images/fav/favicon-196.png">
    <link rel="icon" type="image/png" sizes="160x160" href="http://vibbra.com.br/src/images/fav/favicon-160.png">
    <link rel="icon" type="image/png" sizes="96x96" href="http://vibbra.com.br/src/images/fav/favicon-96.png">
    <link rel="icon" type="image/png" sizes="64x64" href="http://vibbra.com.br/src/images/fav/favicon-64.png">
    <link rel="icon" type="image/png" sizes="32x32" href="http://vibbra.com.br/src/images/fav/favicon-32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="http://vibbra.com.br/src/images/fav/favicon-16.png">
    <link rel="apple-touch-icon" sizes="152x152" href="http://vibbra.com.br/src/images/fav/favicon-152.png">
    <link rel="apple-touch-icon" sizes="144x144" href="http://vibbra.com.br/src/images/fav/favicon-144.png">
    <link rel="apple-touch-icon" sizes="120x120" href="http://vibbra.com.br/src/images/fav/favicon-120.png">
    <link rel="apple-touch-icon" sizes="114x114" href="http://vibbra.com.br/src/images/fav/favicon-114.png">
    <link rel="apple-touch-icon" sizes="76x76" href="http://vibbra.com.br/src/images/fav/favicon-76.png">
    <link rel="apple-touch-icon" sizes="72x72" href="http://vibbra.com.br/src/images/fav/favicon-72.png">
    <link rel="apple-touch-icon" href="http://vibbra.com.br/src/images/fav/favicon-57.png">
    
    <title><?php echo $meta['content']; ?></title>

    <!-- font awesome -->
    <link href="<?php echo base_url() . 'assets/estilos/font-awesome.min.css';?>" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/assets/estilos/alertify.default.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>/assets/estilos/alertify.theme.bootstrap.css" rel="stylesheet">

    <?php 
      foreach($css as $c) {
        echo  "<link href='" . $c['file'] . "' rel='stylesheet'>";
      }
    ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
   <nav class="navbar navbar-custom navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" style="max-width:100px; margin-top: -7px;" href="<?php echo base_url(); ?>">
            <img src="http://vibbra.com.br/src/images/logo.svg"  alt="Logo Vibbrado" class="home-logo-central-link">
          </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse pull-right">
            <ul class="nav navbar-nav">
              <?php 
                foreach($menu as $m) {
                  if ($m['class'] == "dropdown") {
                    echo  "<li class='" . $m['class'] . "'>";
                    echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                    echo $m['name'] . ' <span class="caret"></span>';
                    echo '</a>';
                    echo '<ul class="dropdown-menu">';
                    foreach($m['elements'] as $mel) {
                      echo  "<li class='" . $mel['class'] . "'><a href='" . $mel['link'] . "'>" . $mel['name'] . "</a></li>";
                    }
                    echo '</ul>';
                    echo '</li>';
                  } else {
                    echo  "<li class='" . $m['class'] . "'><a href='" . $m['link'] . "'>" . $m['name'] . "</a>";
                  }
                }
              ?>
            </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>