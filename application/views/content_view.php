<div class="container">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h1 class="blank-space-down text-center">Vibbra.do</h1>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="form-group">
			<select name="user_todos" id="user_todos" class="user-todos form-control">
				<option value="0">Escolha sua lista... ou crie uma nova abaixo</option>
				<option value="1">-- Select One --</option>
				<option value="2">-- Select One --</option>
				<option value="3">-- Select One --</option>
			</select>
			<div class="input-group">
				<input type="text" class="form-control" id="todos_title" placeholder="Título da sua nova lista...">
				<span class="input-group-btn">
					<button id="addTodos" type="button" class="btn btn-default">
						<i class="fa fa-fw fa-plus-circle"></i>
					</button>
				</span>
			</div>
		</div>
	</div>
	<div class="todos-container">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h2 id="todosTitle" class="blank-space-down"></h2>
		</div>
		<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
			<div class="input-group">
				<input id="todoText" type="text" class="form-control" placeholder="Diga o que quer fazer...">
				<span class="input-group-btn">
					<button id="addTodo" type="button" class="btn btn-default">
						<i class="fa fa-fw fa-plus-circle"></i>
					</button>
				</span>
			</div>
			<ul id="todoList" class="list-group">
			</ul>
		</div>
		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
			<ul id="todos_stats" class="list-group">
				<li class="list-group-item">
					<span id="count-donts" class="badge">15</span>
					Por fazer
				</li>
				<li class="list-group-item">
					<span id="count-dos" class="badge">10</span>
					Feitos
				</li>
				<li class="list-group-item">
					<span id="count-total" class="badge">5</span>
					Total
				</li>
			</ul>
			<div class="form-group">
				<label for="input" class="control-label">URL</label>
				<input placeholder="meu-todo-online" type="text" id="todos_url" class="form-control" value="" required="required">
			</div>
			<p id="todos_url_prefix">
				http://vibbrado/dos/<span id="todos_url_preview"></span>
			</p>
			<button type="button" class="btn btn-sm btn-primary">Gravar</button>
		</div>
	</div>
</div>