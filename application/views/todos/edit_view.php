<div class="container">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h1 class="blank-space-down text-center">Vibbra.dos</h1>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="alert alert-info pull-right">
			<strong>
				<?php echo ($todosExiste == TRUE) ? 'ALTERANDO' : 'NOVO'; ?>
			</strong>
		</div>
	</div>
	<div class="todos-container">
		<input type="hidden" id="user_id" value="<?php echo $user_id; ?>">
		<input type="hidden" id="todos_id" value="<?php echo $todos[0]->id; ?>">
		<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
			<div class="input-group">
				<input id="todoText" type="text" class="form-control" placeholder="Diga o que quer fazer...">
				<span class="input-group-btn">
					<button id="addTodo" type="button" class="btn btn-default">
						<i class="fa fa-fw fa-plus-circle"></i>
					</button>
				</span>
			</div>
			<ul id="todoList" class="list-group">
				<?php if (isset($todo)) { foreach ($todo as $key => $td) { ?>
					<li class="list-group-item animated fadeIn">
							<div id="todo_text" contenteditable="true"><?php echo $td->text;?></div>
							<input data-parent="<?php echo $td->parent;?>" class="list-group-box-done"  <?php echo ($td->done == 0) ? '' : 'checked';?> id="<?php echo $td->id;?>" type="checkbox" value="<?php echo ($td->done == 0) ? 'on' : 'off';?>">
							<a href="#" id="removeTodo" class="pull-right">
								<i class="fa fa-trash"></i>
							</a>
							<a href="#" id="addSubTodo" class="pull-right">
								<i class="fa fa-plus-circle"></i>
							</a>
							<ul class="hidden list-group"></ul>
						<?php ?>
					</li>
				<?php  }} ?>
			</ul>
		</div>
		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
			<ul id="todos_stats" class="list-group">
				<li class="list-group-item">
					<span id="count-donts" class="badge"></span>
					Por fazer
				</li>
				<li class="list-group-item">
					<span id="count-dos" class="badge"></span>
					Feitos
				</li>
				<li class="list-group-item">
					<span id="count-total" class="badge"></span>
					Total
				</li>
			</ul>
			<div class="form-group">
				<label for="input" class="control-label">URL</label>
				<input readonly type="text" id="todos_url" class="form-control" value="<?php echo $todosUrl; ?>">
			</div>
			<p id="todos_url_prefix">
				http://vibbrado/dos/<span id="todos_url_preview"><?php echo ($todosExiste == TRUE) ? $todosUrl : ''; ?></span>
			</p>
			<?php if ($todosIsOwnedByUser == true) { ?>
				<button id="removeTodos" type="button" class="btn btn-sm btn-danger">Remover</button>
			<?php } ?>
		</div>
	</div>
</div>