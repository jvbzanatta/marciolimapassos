<div class="container ajaxload">
	<div class="row">
		<div class="col-lg-12">
			<h2>Sobre o Vibbrado...</h2>
			<p>
				Este é o Vibbrado, o seu organizar de tarefas pessoal, que permite 
				a criação rápida e fácil de listas com itens a serem feitos. 
				<br>Suas listas podem ser compartilhadas com colegas, basta passar a url completa e pronto!
			</p>
			<p>
				Divirta-se e mãos à obra!
			</p>
		</div>
	</div>
</div><!-- /.container -->