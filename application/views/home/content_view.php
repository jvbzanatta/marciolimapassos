<div class="container ajaxload">
	<div class="row">
		<div class="col-lg-12">
			<h2>Bem-vindo ao Vibbra.do</h2>
			<p>
				Para criar uma nova lista, clique em Começar,
				ou acesse 
				<ul>
					<li>
						<?php echo base_url() . 'todos/(escolha-uma-url)' ;?>
					</li>
				</ul>
			</p>
			<p>
				Para alterar uma lista existe, acesse direto 
				no endereço: 
				<ul>
					<li>
						<?php echo base_url() . 'todos/(url-existente-a-alterar)'; ?>
					</li>
				</ul>
			</p>
			<p>
				Você pode remover apenas as listas que criou.
			</p>
		</div>
		<div class="col-lg-12">
			<a class="btn btn-primary" data-toggle="modal" href='#comecar_modal'>Começar</a>
			<div class="modal fade" id="comecar_modal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Vamos lá</h4>
						</div>
						<div class="modal-body">
							<form>
								<div class="input-group">
									<input type="text" class="form-control" id="todos_url" value="">
									<span class="input-group-btn">
										<button type="button" class="btn btn-default">Verificar url</button>
									</span>
								</div>
								<p id="todos_url_prefix">
								<?php echo base_url() . 'todos/';?><span id="todos_url_preview"></span>
								</p>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
							<a disabled id="todos_url_go" href="<?php echo base_url('todos'); ?>" role="button" class="disabled btn btn-primary">Criar lista</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- /.container -->